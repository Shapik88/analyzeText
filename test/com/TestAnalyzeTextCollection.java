package com;

import com.AllMethods;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by Евгений on 07.08.2017.
 */
public class TestAnalyzeTextCollection {
    AllMethods allMethods = new AllMethods();

    @Test
    public void read() throws Exception {
        List<String> text = Files.lines(Paths.get("textAnalize.txt"), StandardCharsets.UTF_8).collect(Collectors.toList());

    }
    @Test (expected = NoSuchFileException.class)
    public void readIncorrectPathExceptionTest() throws IOException {
        List<String> text = Files.lines(Paths.get("1.txt"), StandardCharsets.UTF_8).collect(Collectors.toList());
    }



    @Test
    public void output() throws Exception {
        assertEquals(allMethods.output().get(0), "Many");
    }


    @Test
    public void sort() throws Exception {
        assertEquals(allMethods.sort(allMethods.punctuationDell(allMethods.output())).get(0), "and");
       }



}
