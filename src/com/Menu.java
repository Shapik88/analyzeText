package com;


import java.util.Scanner;

import java.io.*;


public class Menu {

    AllMethods allMethods = new AllMethods();


    public void textMenu(){
        System.out.println("|---------------------------------------------------|");
        System.out.println("| If you want to text on the screen enter 1         |");
        System.out.println("| Remove words with lenght < = 3 enter 2            |");
        System.out.println("| If you want to sort, click enter 3                |");
        System.out.println("| If you want dell punctuation marks enter 4        |");
        System.out.println("| enter 5                                           |");
        System.out.println("| If you want to quit enter 6                       |");
        System.out.println("|---------------------------------------------------|");
    }

    public void menuSwitch(String decision) throws IOException {

        switch (decision){

            case "1":
                allMethods.printList();
                break;
            case "2":
                allMethods.remove(allMethods.punctuationDell(allMethods.output())).forEach(element -> System.out.println(element));
                break;
            case "3":
                allMethods.sort(allMethods.punctuationDell(allMethods.output())).forEach(element -> System.out.println(element));
                break;

            case "4":
                allMethods.punctuationDell(allMethods.read()).forEach(element -> System.out.println(element));
                break;
//            case "5":
//                allMethods.countWords(allMethods.read()).forEach(element -> System.out.println(element));
//                break;
            case "6":
                System.out.println("Exit");
                break;
            default:
                System.out.println("Error");
                break;
        }

    }

}

