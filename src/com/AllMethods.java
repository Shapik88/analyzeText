package com;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;




public class AllMethods {

      String[] arr;
    public List<String> read() throws IOException {
        List<String> text = Files.lines(Paths.get("textAnalize.txt"), StandardCharsets.UTF_8).collect(Collectors.toList());
        return text;
    }
    public List<String> output() throws IOException {
        List<String> outputs = new ArrayList<>();
        read().forEach( element -> outputs.addAll(Arrays.asList(element.split(" "))));
        return outputs;
    }

    public List<String> printList() throws IOException {

        List<String> prList =  read().stream()
                .collect(Collectors.toList());
        prList.forEach(element -> System.out.println(element));

        return prList;

    }
    public List<String> punctuationDell(List<String> list) throws IOException {

        List<String> clearSplit = new ArrayList<>();
        list.forEach(element -> clearSplit.addAll(Arrays.asList(element
                .replace("-", "")
                .replace(".", "")
                .replace(",", "")
                .toLowerCase()

        )));

        return clearSplit;
    }

    public List<String> sort(List<String> list) throws IOException {
        List<String> sortedList = list.stream().sorted((o1, o2) -> o1.toString().compareTo(o2.toString())).collect(Collectors.toList());
        return sortedList;
    }

//
//        public  List<String>  countWords(List<String> list){
//
//            List<String>  result = new ArrayList<>();
//            for (int i = 0; i < result.size(); i++) {
//                String slovo = result.get(i);//разбиваем лист на строки
//                int num = 0;//число для каждого слова
//                for (int j = 0; j < result.size(); j++) {
//                    if (slovo.equals(result.get(j)))//подсчет количества слов
//                    {
//                        num++;
//                    }
//                    System.out.println(slovo + " 1" + num);
//
//                }
//            }
//            return result;
//        }


    public List<String> remove(List<String> list) throws IOException {
        List<String> listDellAgr = list.stream().filter(element -> element.length()>3).collect(Collectors.toList());
        System.out.println("текст без слов меньше  3! ");
        return listDellAgr;
    }

}
